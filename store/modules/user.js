import {
	login
} from '@/api/auth';
import {
	toast
} from '@/utils/toast';

const state = {
	userinfo: {},
	isLogin: false,
	OTCUSDT: {},
	OTCUSDT1: {},
	IsOpen: {},
	contractid: {},
	COIN: {},
	LATESTPRICE: {}
}
const mutations = {
	LOGIN_INFO: (state, info) => {
		state.userinfo = info;
		state.isLogin = true;
		uni.setStorage({
			key: 'userinfo',
			data: info
		})
	},
	// 通道一
	OTCUSDT(state, info) {
		state.OTCUSDT = info;
		uni.setStorage({
			key: 'OTCUSDT',
			data: info
		})
	},
	// 通道二
	OTCUSDT1(state, info) {
		state.OTCUSDT1 = info;
		uni.setStorage({
			key: 'OTCUSDT1',
			data: info
		})
	},
	// 合约//
	contractid(state, info) {
		state.contractid = info;
		uni.setStorage({
			key: 'contractid',
			data: info
		})
	},
	COIN(state, info) {
		state.COIN = info;
		uni.setStorage({
			key: 'COIN',
			data: info
		})
	},
	// 最新价
	LATESTPRICE(state, info) {
		state.LATESTPRICE = info;
		uni.setStorage({
			key: 'LATESTPRICE',
			data: info
		})
	},




	// 判断是否开放币种
	IsOpen(state, info) {
		state.IsOpen = info;
		uni.setStorage({
			key: 'IsOpen',
			data: info
		})
	},
	logout(state) { //退出登录 
		console.log(state)
		state.userinfo = {}
		state.isLogin = false;
		uni.removeStorageSync('userinfo');
	},
	listCountryaz: (state, listCountryaz) => {
		state.listCountryaz = listCountryaz;
	},
	changeLoginStatus(state, isLogin) {
		state.isLogin = isLogin;
	}
}
const actions = {
	changeLoginStatus: ({
		commit
	}, Bool) => {
		commit('changeLoginStatus', Bool)
	},
	loginInfo: ({
		commit
	}, form) => {
		login({ ...form
			})
			.then(res => {
				try {
					var info = JSON.parse(res.data);
				} catch (e) {
					var info = res.data;
				}
				if (info.code == 200) {
					toast('登录成功');
					uni.setStorage({
						key: 'flag',
						data: true
					})
					uni.navigateTo({


						url: '/pages/quotation/main',
					})
					commit('LOGIN_INFO', info.data)
				} else {
					toast(info.msg)
				}
			})
	},
	listCountryaz: ({
		commit
	}, form) => {
		listCountryaz()
			.then(res => {
				if (res.data.code == 200) {
					commit('listCountryaz', res.data.data)
				}
			})
	}
}

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
