import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import createPersistedState from "vuex-persistedstate"
import  request from '@/utils/request';

Vue.use(Vuex)
// https://webpack.js.org/guides/dependency-management/#requirecontext
const modulesFiles = require.context('./modules', true, /\.js$/)

// you do not need `import app from './modules/app'`
// it will auto require all vuex module from modules file
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
	// set './app.js' => 'app'
	const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')

	const value = modulesFiles(modulePath)
	modules[moduleName] = value.default
	return modules
}, {})


// 国际化相关代码
try {
	// 1. 分析用户已经选择的语言 
	var userLang = uni.getStorageSync("userLang");
	
	// 2. 如果用户没有选择过获取用户手机的语言
	if (!userLang) {
		const sys = uni.getSystemInfoSync();
		userLang = sys.language
       // console.log(userLang,999)
	   
	   
	}
	   uni.setStorage({
			key: 'userLang',
			data: userLang
		}),
	console.log(userLang, "语言");
	// 以中英文切换为例, 其他语言请使用 getSystemInfoSync 获取语言对应的字符串
	// 然后扩展语言包即可
	if (userLang.Accountsecurity == "Accountsecurity") {
		var lang = require('../language/en.js');
	} else {
		
		var lang = require('../language/zh.js');
	}
} catch (e) {
	// error
}

const store = new Vuex.Store({
	modules,
	getters,
	state: {
		lang: lang
	},
	mutations: {
		changeLang: function(state) {

			uni.showActionSheet({
				itemList: ['简体中文', 'English'],
				success: function(e) {
					if (e.tapIndex == 0) {
						uni.setStorage({
								key: 'userLang',
								data: lang
							}), 
							
						lang = require('../language/zh.js');
                        uni.setStorage({
                        		key: 'userLang',
                        		data: lang
                        	}),
                        	
                        lang = require('../language/zh.js');
						request('/app/user/language', 'GET', {
								language:0
							})
							.then(res => {
								console.log(res.data,"中文")
								if (res.data.code == 200) {
									
								}else{
									
								}
								
							})

					} else {
						uni.setStorage({
								key: 'userLang',
								data: lang
							}),
							
						lang = require('../language/en.js');
                        uni.setStorage({
                        		key: 'userLang',
                        		data: lang
                        	}),
                        	
                        lang = require('../language/en.js');
						request('/app/user/language', 'GET', {
								language:1
							})
							.then(res => {
								console.log(res.data,"英文")
								if (res.data.code == 200) {
									
								}else{
									
								}
								
							})
					}
					state.lang = lang;
				}
			})
		}
	}
})

export default store
