
// 个人
const baseURL = 'https://www.orient-trade.com/exchange'
import {toast} from '@/utils/toast'


const request = function(url, method, data,isSys=false) {
 return new Promise((resove,reject)=>{

	 	uni.request({
	 		header: {
	 			'content-type': 'application/x-www-form-urlencoded' //自定义请求头信息
	 		},
	 		dataType:'JSON',
	 		url: baseURL + url,
	 		method,
	 		data,
			success:function(data){
				if(data.statusCode>=500){
					// toast('服务异常,请稍后再试');
				}
				if(data.data.code!=200 && data.data.code!=undefined){
					toast(data.data.msg);
				}
				if(data.data.code == 225){	
					toast('登录过期请重新登录')
					uni.navigateTo({
					    url: '/pages/auth/login'
					});
				}
				resove(data)
				
			},
	 		fail:function(res){
				console.log(res)
				
	 			// toast('系统维护中');
	 			uni.hideLoading();
	 		}
	 	})
	
 })
	 
}


export default request

