import request from '@/utils/Promises'

export function register(form){ 
	return request(`user/register`,'POST',{...form})
}
export function sendCode({type,number,areaNo}){
	var numberKey;
	numberKey= type !=0? 'phoneNo':'mailbox';
	let numberForm={phoneNo:number,mailbox:number,areaNo};
	delete numberForm[numberKey]
	if(type==1){
		delete numberForm["areaNo"];
	}
	return request('/user/sendCode',"GET",{type,...numberForm})	
}


export function login(form){

	return request('/app/user/login',"POST",{...form})	
}

