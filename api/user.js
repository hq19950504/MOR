import { getToken } from '@/utils/server.js';
import request from '@/utils/Promises'

export function basisInformation(){ 
	return request('/user/basisInformation','GET',{token:getToken()})
}

export function setAssterPass(form){
	return request('/user/setTransactionPassword','POST',{...form,token:getToken()})
}

export function bindPhone(form){
	return request('/user/updatePhoneNo','POST',{...form,token:getToken()})
}
export function bindEmail(form){
	return request('/user/emailAuthentication','POST',{...form,token:getToken()})
}


export function getAreaCode(phoneNo){
	return request('/user/getAreaCode','GET',{phoneNo})
}

export function verifyIdentidy(form){
	return request('/user/verifyIdentidy','POST',{...form})
}

export function forgetPassword(form){
	return request('/user/forgetPassword','POST',{...form})
} 
export function editPass(form){
	return request('/user/updatePassword','POST',{...form,token:getToken()})
}

