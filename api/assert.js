
const baseURL = ` https://www.orient-trade.com/quotes`;//行情币币
const baseURL1 = `https://www.orient-trade.com/trade-pool`; //币币
const URL = `https://www.orient-trade.com/exchange`; //个人
const promiseURL = `https://www.orient-trade.com/quotesDBUpdateBiBi`;//合约行情
const promiseURL1 = `https://www.orient-trade.com/contract`;//合约
import  request from '@/utils/request';

import {toast} from '@/utils/toast'
import { getToken } from '@/utils/server.js';

const Pagerequest = function(url, method, data) {
 return new Promise((resove,reject)=>{
	 	uni.request({
	 		header: {
	 			'content-type': 'application/x-www-form-urlencoded' ,//自定义请求头信息
				 // "Content-Type": "application/json; charset=UTF-8"
	 		},
	 		dataType:'json',
	 		url: url,
	 		method,
	 		data,
			success:function(data){
				if(data.statusCode>=500){
					// toast('服务异常,请稍后再试');
				}
				if(data.data.code!=200 && data.data.code!=undefined){
					toast(data.data.msg);
				}
				resove(data)
			},
	 		fail:function(err){
				console.log(err)
				reject('服务异常')
	 			// toast('服务异常,请稍后再试');
	 			uni.hideLoading();
	 		}
	 	})
	
 })
	 
}

// 盘口倒序
export function getDepthByTwoCoins(form){
	return Pagerequest(baseURL+'/quotations/getBuySellData',"GET",{...form})	
}

export function userVirtualWallet(virtuaWalletName){
	if(virtuaWalletName){
		return  Pagerequest(URL+"/app/userVirtualWallet/userVirtualWallet",'GET',{token:getToken(),virtuaWalletName})
	}else{
		return  Pagerequest(URL+"/app/userVirtualWallet/userVirtualWallet",'GET',{token:getToken()})
	}
}

export function getMotherCoinName(){
	return  Pagerequest(baseURL+"/quotations/getMotherCoinName",'GET',{token:getToken()})
}

export function getlist(coinName) {
	return Pagerequest(baseURL+'/quotations/get24hrDataByMotherCoinName', "GET", {motherCoinName: coinName});
}


export function addTrade(form){
	return Pagerequest(baseURL1+ '/tradePool/add','POST',{token:getToken(),...form})
}

// 交易对行情
export function DataByTwoCoinName(form){
	
	return Pagerequest(baseURL+'/quotations/get24hrDataByTwoCoinName','GET',{...form})
}

export function  regularAnnouncement(){
	return Pagerequest(URL+'/message/regularAnnouncement','GET',{page:1,rows:6})
}
export function  getExchangeRateByCoinName(data){
	return Pagerequest(baseURL+'/quotesExchangeRate/getExchangeRateByCoinName','GET',data)		
}

export function get24hrDataForAll(data){
	return Pagerequest(baseURL+'/quotations/get24hrDataForAll',"GET",data)	
}
// 查看合约币种
export function get24hrDataForAgreement(data){
	return Pagerequest(promiseURL+'/quotations/get24hrDataForAgreement',"GET",data)	
}
// 合约盘口
export function getBuySellData(data){
	return Pagerequest(promiseURL+'/quotations/getBuySellData',"GET",data)	
}
// 合约最新价
export function get24hrDataByTwoCoinName(data){
	return Pagerequest(promiseURL+'/quotations/get24hrDataByTwoCoinName',"GET",data)	
}
// 添加合约买入开多
export function add(data){
	return Pagerequest(promiseURL1+'/contractEntrust/add',"POST",data)	
}



// 持仓
export function contractWarehouse(data){
	return Pagerequest(promiseURL1+'/contractWarehouse/list',"POST",data)	
}
// 合约钱包
export function VirtualWallet(data){
	return Pagerequest(promiseURL1+'/contractWallet/get/',"POST",data)	
}


// 根据交易币种ID获取（上架状态的）合约
export function getContract(data){
	return Pagerequest(promiseURL1+'/contract/getContract',"POST",data)	
}

// 查看币种汇率
export function getExchangeRateBy(data){
	return Pagerequest(promiseURL+'/quotesExchangeRate/getExchangeRateByCoinName',"GET",data)	
}

// 划转
export function transfer(data){
	return Pagerequest(promiseURL1+'/contractWallet/transfer',"POST",data)	
}
// 当前委托
export function contractList(data){
	return Pagerequest(promiseURL1+'/contractEntrust/list',"POST",data)	
}
//可用
export function getAvailableUsdt(data){
	return Pagerequest(URL+'/virtualWallet/getAvailableUsdt',"GET",data)	
}
// 平仓记录
export function list(data){
	return Pagerequest(promiseURL1+'/contractWarehouse/history',"POST",data)	
}
// 修改止盈止损
export function update(data){
	return Pagerequest(promiseURL1+'/contractWarehouse/update',"POST",data)	
}
// 合约财务列表
export function userContract(data){
	return Pagerequest(promiseURL1+'/contractWallet/record',"POST",data)	
}

export function get24hr(data){
	
	return Pagerequest(promiseURL1+'/quotations/get24hrDataByTwoCoinName','GET',data)
}
// 扯单
export function contractupdate(data){
	
	return Pagerequest(promiseURL1+'/contractEntrust/update','POST',data)
}
// 手动平仓
export function closeWarehouse(data){
	
	return Pagerequest(promiseURL1+'/contractWarehouse/close','POST',data)
}
// 币对是否开放(传币种ID对或者币种名称对)
export function  isOpen(data){
	return Pagerequest(URL+'/revisedTradePool/isOpen','GET',data)
}
// 合约权益
export function contractIncome(data){
	
	return Pagerequest(promiseURL1+'/userContractVirtualWallet/contractIncome','GET',data)
}
// 持仓详情
export function detail(data){
	
	return Pagerequest(promiseURL1+'/contractWarehouse/detail','POST',data)
}
// 合约账户信息
export function account(data){
	
	return Pagerequest(promiseURL1+'/contract/account','POST',data)
}
// 当前委托止盈止损
export function updateTakeprofit(data){
	
	return Pagerequest(promiseURL1+'/contractEntrust/updateTakeprofit','POST',data)
}




